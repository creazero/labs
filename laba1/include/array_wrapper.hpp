class ArrayWrapper
{
public:
    // Конструкторы. Будет часть задания про
    // перегрузку конструкторов. Перегрузка конструкторов --
    // просто наличие нескольких конструкторов.
    ArrayWrapper(int *vec, int size)
        : vector(vec), size_(size) {}
    ArrayWrapper(int **mat, int size)
        : matrix(mat), size_(size) {}

    // Деструктор. Освобождаем занятую память
    ~ArrayWrapper();

    // Транспонирование матрицы
    void transpose();

    // Макс. значение в векторе
    int max();
    // Мин. значение в векторе
    int min();

    // Функции класса для вывода
    void print_matrix();
    void print_vector();

    // nullptr - указатель указывает на пустоту
    int *vector = nullptr;
    int **matrix = nullptr;
    int size_; // Размер массива (одного из)
};

// Функции-помощники для генерации
// матрицы и вектора
int *generate_vector(int size);
int **generate_matrix(int size);