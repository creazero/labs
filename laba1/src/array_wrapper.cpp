#include <cstdlib> // Для рандома
#include <iostream>

#include "array_wrapper.hpp"

using namespace std;

ArrayWrapper::~ArrayWrapper()
{
    if (matrix != nullptr) {
        for (int i = 0; i < size_; i++) {
            delete[] matrix[i];
        }
        delete[] matrix;
    }
    if (vector != nullptr) delete[] vector;
    size_ = 0;
}

void ArrayWrapper::transpose()
{
    int **new_matrix = new int*[size_];
    for (int i = 0; i < size_; i++) {
        new_matrix[i] = new int[size_];
        for (int j = 0; j < size_; j++) {
            new_matrix[i][j] = matrix[j][i];
        }
    }
    // Очищаем память, занятую изначальной матрицей
    for (int i = 0; i < size_; i++) {
        delete[] matrix[i];
    }
    delete[] matrix;
    matrix = new_matrix;
}

int **generate_matrix(int size)
{
    int **matrix = new int*[size];
    for (int i = 0; i < size; i++) {
        matrix[i] = new int[size];
        for (int j = 0; j < size; j++) {
            matrix[i][j] = rand() % 50;
        }
    }
    return matrix;
}

int *generate_vector(int size)
{
    int *vector = new int[size];
    for (int i = 0; i < size; i++) {
        vector[i] = rand() % 25 - 10;
    }
    return vector;
}

void ArrayWrapper::print_matrix()
{
    for (int i = 0; i < size_; i++) {
        for (int j = 0; j < size_; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

void ArrayWrapper::print_vector()
{
    for (int i = 0; i < size_; i++) {
        cout << vector[i] << " ";
    }
    cout << endl;
}

int ArrayWrapper::max()
{
    // Мин. число -10
    int max = -11;
    for (int i = 0; i < size_; i++) {
        if (vector[i] > max)
            max = vector[i];
    }
    return max;
}

int ArrayWrapper::min()
{
    // Макс. число 15
    int min = 16;
    for (int i = 0; i < size_; i++) {
        if (vector[i] < min)
            min = vector[i];
    }
    return min;
}