#include <iostream>
#include <ctime>
#include <cstdlib>

#include "array_wrapper.hpp"

using namespace std;

int main()
{
    srand(time(nullptr));

    int **matrix = generate_matrix(5);
    ArrayWrapper aw(matrix, 5);
    cout << "Before:" << endl;
    aw.print_matrix();
    aw.transpose();
    cout << "After:" << endl;
    aw.print_matrix();

    int *vector = generate_vector(10);
    ArrayWrapper aw_v(vector, 10);
    aw_v.print_vector();
    cout << "Max: " << aw_v.max() << endl;
    cout << "Min: " << aw_v.min() << endl;
    return 0;
}