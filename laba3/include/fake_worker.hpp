#ifndef FAKE_WORKER_HPP
#define FAKE_WORKER_HPP

#include "matrix_worker.hpp"

class FakeWorker : public MatrixWorker
{
public:
    FakeWorker(double **matrix, int rows, int columns)
        : MatrixWorker(matrix, rows, columns) {}

    void division(double) override;
};

#endif