#ifndef MATRIX_WORKER_HPP
#define MATRIX_WORKER_HPP

class MatrixWorker
{
public:
    MatrixWorker(double **matrix, int rows, int columns)
        : matrix_(matrix), rows_{rows}, columns_{columns} {}

    virtual void division(double);
    void sort_columns();
    void print();

    static double **generate_matrix(int, int);

protected:
    double **matrix_;
    int rows_;
    int columns_;
};

#endif