#include "matrix_worker.hpp"

#include "fake_worker.hpp"

#include <ctime>
#include <cstdlib>
#include <iostream>

int main()
{
    std::srand(std::time(nullptr));
    int n = 0,
        m = 0;
    std::cout << "Rows: ";
    std::cin >> n;
    std::cout << "Cols: ";
    std::cin >> m;

    auto matrix = MatrixWorker::generate_matrix(n, m);
    MatrixWorker mw(matrix, n, m);
    mw.print();
    mw.sort_columns();
    mw.print();

    int magic = 0;
    std::cout << "Magic: ";
    std::cin >> magic;

    mw.division(magic);
    mw.print();

    FakeWorker fw(matrix, n, m);
    fw.print();
    fw.division(magic);
    fw.print();
    return 0;
}