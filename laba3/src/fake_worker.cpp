#include "fake_worker.hpp"

void FakeWorker::division(double magic)
{
    bool flag = false;
    for (int i = 0; i < columns_; i++) {
        if (matrix_[0][i] < magic) {
            flag = true;
        }
    }
    if (!flag) return;
    for (int i = 0; i < columns_; i++) {
        matrix_[0][i] /= matrix_[rows_-1][i];
    }
}