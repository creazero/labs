#include "matrix_worker.hpp"

#include <iostream>
#include <cstdlib>

void MatrixWorker::sort_columns()
{
    for (int i = 0; i < columns_; i++) {
        double max = matrix_[0][i];
        int max_pos = 0;
        for (int j = 1; j < rows_; j++) {
            if (matrix_[j][i] > max) {
                max = matrix_[j][i];
                max_pos = j;
            }
        }
        if (max_pos != 0) {
            matrix_[max_pos][i] = matrix_[0][i];
            matrix_[0][i] = max;
        }
    }
}

void MatrixWorker::division(double magic)
{
    bool flag = false;
    for (int i = 0; i < columns_; i++) {
        if (matrix_[0][i] < magic) {
            flag = true;
        }
    }
    if (!flag) return;
    for (int i = 0; i < columns_; i++) {
        matrix_[rows_-1][i] /= matrix_[0][i];
    }
}

void MatrixWorker::print()
{
    for (int i = 0; i < rows_; i++) {
        for (int j = 0; j < columns_; j++) {
            std::cout << matrix_[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

double ** MatrixWorker::generate_matrix(int rows, int cols)
{
    auto result = new double*[rows];
    for (int i = 0; i < rows; i++) {
        result[i] = new double[cols];
        for (int j = 0; j < cols; j++) {
            result[i][j] = std::rand() % 50;
        }
    }
    return result;
}