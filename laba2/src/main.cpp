#include <cstdlib>
#include <ctime>
#include <iostream>

double **generate_matrix(int, int);
void sort_columns(double **, int, int);
void division(double **, int n, int m, double);

void print(double **, int, int);

int main()
{
    std::srand(std::time(nullptr));
    int n = 0,
        m = 0;
    std::cout << "Enter number of rows of a matrix: ";
    std::cin >> n;
    std::cout << "Enter number of columns of a matrix: ";
    std::cin >> m;
    auto matrix = generate_matrix(n, m);
    print(matrix, n, m);
    sort_columns(matrix, n, m);
    print(matrix, n, m);

    double magic = 0;
    std::cout << "\"Заданная\" величина: ";
    std::cin >> magic;

    division(matrix, n, m, magic);
    print(matrix, n, m);

    return 0;
}

double **
generate_matrix(int n, int m)
{
    auto result = new double*[n];
    for (int i = 0; i < n; i++) {
        result[i] = new double[m];
        for (int j = 0; j < m; j++) {
            result[i][j] = std::rand() % 50;
        }
    }
    return result;
}

void
sort_columns(double **matrix, int rows, int columns)
{
    for (int i = 0; i < columns; i++) {
        double max = matrix[0][i];
        int max_pos = 0;
        for (int j = 1; j < rows; j++) {
            if (matrix[j][i] > max) {
                max = matrix[j][i];
                max_pos = j;
            }
        }
        if (max_pos != 0) {
            matrix[max_pos][i] = matrix[0][i];
            matrix[0][i] = max;
        }
    }
}

void print(double **matrix, int rows, int columns)
{
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            std::cout << matrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void division(double **matrix, int rows, int columns, double magic)
{
    bool flag = false;
    for (int i = 0; i < columns; i++) {
        if (matrix[0][i] < magic) {
            flag = true;
        }
    }
    if (!flag) return;
    for (int i = 0; i < columns; i++) {
        matrix[rows-1][i] /= matrix[0][i];
    }
}